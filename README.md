# add_rsa_key

Create new rsa key
```bash
ssh-keygen -o -t rsa -b 4096 -C "<email_to_login_gitlab@email.com>" -f ~/.ssh/<name_rsa>
```

Add key to gitlab
```bash
SSh Keys> Add an SSH key
```

Show data rsa key
```bash
cat ~/.ssh/<name_rsa>.pub
```
Copy data
```bash
ssh-rsa <data content rsa.pub>
```

Create config
```bash
nano ~/.ssh/config
```
Paste config
```bash
Host gitlab.com
  Preferredauthentications publickey
  IdentityFile ~/.ssh/<name_rsa>.pub
```